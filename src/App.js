import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import ColorThief from 'color-thief-browser';

class App extends Component {

  myNode = null;

  // componentDidMount() {

  //   this.colorPicker = new ColorThief();

  // }


  render() {
    return (
      <div>
        <img
          src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/48.png"}
          alt={"No picture"}
          ref={ref => (this.myNode = ref)}
          crossOrigin=" "
          onLoad={() => {
            let colorPicker = new ColorThief();
            let color = colorPicker.getColor(this.myNode);
            console.log(color);
            let bodyNode = document.querySelector('body');
            bodyNode.style.cssText = `background-color: rgb(${color.r}, ${color.g}, ${color.b});`;
          }}
        />
      </div>
    );
  }
}

export default App;
